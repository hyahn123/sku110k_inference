class Product:
    def __init__(self, sku, product_name, segment, shelf, order, facing):
        self.sku = sku
        self.name = product_name
        self.segment = segment
        self.shelf = shelf
        self.order = order
        self.facing = facing

    def get_info(self):
        return(self.sku, self.product_name, self.segment, self.shelf, self.order, self.facing)

