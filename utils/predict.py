import keras
import numpy as np
import time
import tensorflow as tf
from object_detector_retinanet.keras_retinanet import models
from object_detector_retinanet.keras_retinanet.utils import EmMerger
from object_detector_retinanet.keras_retinanet.utils.visualization import draw_detections
from object_detector_retinanet.utils import create_folder, root_dir

def predict(image, scale, image_name, model, thresh=.3, hard_score_rate=1., max_detections=9999):
    # run network - object_detector_retinanet.keras_retinanet
    start = time.time()
    boxes, hard_scores, labels, soft_scores = model.predict_on_batch(np.expand_dims(image, axis=0))
    processing_time = time.time() - start
    soft_scores = np.squeeze(soft_scores, axis=-1)
    soft_scores = hard_score_rate * hard_scores + (1 - hard_score_rate) * soft_scores

    # correct boxes for image scale
    boxes /= scale

    # select indices which have a score above the threshold
    indices = np.where(hard_scores[0, :] > thresh)[0]

    # select those scores
    scores = soft_scores[0][indices]
    hard_scores = hard_scores[0][indices]

    # find the order with which to sort the scores
    scores_sort = np.argsort(-scores)[:max_detections]

    # select detections
    image_boxes = boxes[0, indices[scores_sort], :]
    image_scores = scores[scores_sort]
    image_hard_scores = hard_scores[scores_sort]
    image_labels = labels[0, indices[scores_sort]]
    image_detections = np.concatenate(
        [image_boxes, np.expand_dims(image_scores, axis=1), np.expand_dims(image_labels, axis=1)], axis=1)
    results = np.concatenate(
        [image_boxes, np.expand_dims(image_scores, axis=1), np.expand_dims(image_hard_scores, axis=1),
         np.expand_dims(image_labels, axis=1)], axis=1)
    filtered_data = EmMerger.merge_detections(image_name, results)
    filtered_boxes =[]
    filtered_scores = []
    filtered_labels = []

    for ind, detection in filtered_data.iterrows():
        box = np.asarray([detection['x1'], detection['y1'], detection['x2'], detection['y2']])
        filtered_boxes.append(box)
        filtered_scores.append(detection['confidence'])
        filtered_labels.append('{0:.2f}'.format(detection['hard_score']))

    return filtered_boxes, filtered_scores, filtered_labels
