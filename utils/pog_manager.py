import pandas as pd
import csv
import numpy as np
import copy
from .product import Product

class POGManager:
    POG_CSV = 'pog.csv'
    segments = 4
    shelves = 6

    def __init__(self):
        #self.product_map = self.__create_product_map()
        self.pog_map = self.__create_product_map()

    def __create_product_map(self):
        loc_df = self.__create_location_df()
        return self.__merge_product_info(loc_df)

    def __create_header(self, start, end):
        arr = []
        for i in range(start, end):
            arr.append(str(i))
        return arr

    def __create_df(self):
        columns = self.__create_header(1, self.segments+1)
        rows = self.__create_header(1, self.shelves+1)
        df = pd.DataFrame(index=rows, columns=columns)
        return df

    def __create_product_map(self):
        df = self.__create_df()
        with open(self.POG_CSV, 'r', encoding='UTF8') as csv_file:
            reader = csv.reader(csv_file)
            for row in reader:
                sku, name, seg, shelf, order, facing = row[14:20]
                product = Product(sku, name, seg, shelf, order, facing)
                if df[seg][shelf] is np.nan:
                    df[seg][shelf] = (product,)
                else:
                    l = list(df[seg][shelf]+(product,))
                    l.sort(key=lambda element:element.order)
                    df[seg][shelf] = tuple(l)
        return df

    def get_pog_map(self):
        return self.pog_map

    def get_product_list(self, seg, shelf):
        return list(self.pog_map[str(seg)][str(shelf)])

    def get_product_group_count(self, seg, shelf):
        return len(self.pog_map[str(seg)][str(shelf)])

    def get_facing_count(self, seg, shelf):
        l = list(self.pog_map[str(seg)][str(shelf)])
        count = 0
        for p in l:
            count += int(p.facing)
        return count

    def get_segment(self, seg):
        return list(self.pog_map[str(seg)][:])
