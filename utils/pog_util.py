import cv2
import csv
import numpy as np
from utils.colors import get_mean_color, delta

def load_shelf_loc(csv_file_name):
    shelf_loc = {}
    with open(csv_file_name, 'r') as csv_file:
        reader = csv.reader(csv_file)
        for row in reader:
            seg, shelf, x1, y1, x2, y2 = row
            shelf_loc[(seg, shelf)] = tuple(map(int, (x1, y1, x2, y2)))
    return shelf_loc

shelf_loc = load_shelf_loc('map.csv')

def compute_iou(box_a, box_b):
    x_a = max(box_a[0], box_b[0])
    y_a = max(box_a[1], box_b[1])
    x_b = min(box_a[2], box_b[2])
    y_b = min(box_a[3], box_b[3])
    inter_area = abs(max((x_b - x_a, 0)) * max((y_b - y_a), 0))
    if inter_area == 0:
        return 0
    box_a_area = compute_rect_area(box_a)
    box_b_area = compute_rect_area(box_b)

    iou = inter_area / float(box_a_area + box_b_area - inter_area)
    return iou

def compute_rect_area(box):
    return abs((box[2]-box[0])*(box[3]-box[1]))

def check_contains(box_a, box_b):
    area_a = compute_rect_area(box_a)
    area_b = compute_rect_area(box_b)
    if area_a > area_b:
        return True
    else:
        return False

def find_overlapping_boxes(area, boxes, thresh):
    overlapping_boxes = []
    for box in boxes:
        iou = compute_iou(area, box)
        if iou > thresh:
            overlapping_boxes.append(box)
    return overlapping_boxes

def retrieve_groups(img, boxes):
    group_boxes = []
    for loc, coord in shelf_loc.items():
        l = find_overlapping_boxes(coord, boxes, 0.04)
        l.sort(key=lambda element:element[0])
        cluster = []
        for i in range(len(l)):
            box = l[i]
            if not i in cluster:
                cluster = []
                for j in range(i+1, len(l)):
                    r_box = l[j]
                    current_img = img[box[1]:box[3], box[0]:box[2]] 
                    right_img = img[r_box[1]:r_box[3], r_box[0]:r_box[2]] 

                    distance = delta(get_mean_color(current_img),
                                     get_mean_color(right_img))
                    if distance < 10:
                        cluster.append(j)
                cluster.append(i)
                cluster.sort()
                xmin = l[cluster[0]][0]
                xmax = l[cluster[-1]][2]
                group_boxes.append((loc, (xmin, coord[1], xmax, coord[3])))
    return group_boxes

def remove_overlapping_box(group_boxes):
    output = {}
    overlapping_index = []
    for i in range(len(group_boxes)):
        loc, box_a = group_boxes[i]
        seg, shelf = loc
        if not i in overlapping_index:
            overlapping_index = []
            for j in range(i+1, len(group_boxes)):
                loc_b, box_b = group_boxes[j]
                iou = compute_iou(box_a, box_b)
                if iou > 0.2:
                    overlapping_index.append(j)
                    rect = box_a if check_contains(box_a, box_b) else box_b
            if not overlapping_index:
                rect = box_a
            
            if (seg, shelf) in output:
                l = list(output[(seg, shelf)])
                l.append(rect)
                output[(seg, shelf)] = tuple(l)
            else:
                output[(seg, shelf)] = (rect, )
    return output

def find_groups(img, boxes):
    groups = retrieve_groups(img, boxes)
    output = remove_overlapping_box(groups)
    return output
