import cv2
import random
import numpy as np
from colormath.color_objects import sRGBColor, LabColor
from colormath.color_conversions import convert_color
from colormath.color_diff import delta_e_cie2000

def get_random_color():
    return (random.randint(0, 255),
            random.randint(0, 255),
            random.randint(0, 255))

def get_mean_color(img):
    return img.mean(axis=0).mean(axis=0)

def rgb2lab(rgb):
    srgb = [float(c/255) for c in rgb]
    srgb = sRGBColor(srgb[0], srgb[1], srgb[2])
    return convert_color(srgb, LabColor)

def rgb2bgr(rgb):
    return cv2.cvtColor(rgb, cv2.COLOR_RGB2BGR) 

def delta(avg_color, min_color):
    lab_avg = rgb2lab(avg_color)
    lab_min = rgb2lab(min_color)
    return delta_e_cie2000(lab_avg, lab_min)
