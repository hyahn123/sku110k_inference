import os
os.environ["CUDA_VISIBLE_DEVICES"]="1"
import cv2
import keras
import tensorflow as tf
import numpy as np
import glob
import utils.pog_util as util
from PIL import Image, ImageFont, ImageDraw
from keras_retinanet.utils.image import read_image_bgr, preprocess_image, resize_image
from object_detector_retinanet.keras_retinanet import models
from object_detector_retinanet.keras_retinanet.utils.visualization import draw_detections
from utils.pog_manager import POGManager
from utils.predict import predict
from utils.colors import get_random_color

font = ImageFont.truetype('./nanum.ttf', 15)

def make_dir(dir_name):
    if not os.path.exists(dir_name):
        os.mkdir(dir_name)

cluster_output_dir = 'cluster_output'
make_dir(cluster_output_dir)
detection_output_dir = 'detection_output'
make_dir(detection_output_dir)

def get_session():
    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True
    return tf.Session(config=config)

def draw_bbox(img, box):
    x1, y1, x2, y2 = box
    dr = ImageDraw.Draw(img, 'RGBA')
    color = get_random_color()
    dr.rectangle(((x1, y1), (x2, y2)), fill=color+(100,))

def draw_name(img, box, name):
    x1, y1, x2, y2 = box
    dr = ImageDraw.Draw(img, 'RGBA')
    center_w = int((x2-x1)/2)+x1 - 30
    center_h = int((y2-y1)/2)+y1
    dr.text((center_w, center_h), name, font=font, fill=(255, 255, 255))

keras.backend.tensorflow_backend.set_session(get_session())
model_path = os.path.join('.', 'iou_resnet50_csv_06.h5')
# load model - object_detector_retinaneet.keras_retinanet
model = models.load_model(model_path, backbone_name='resnet50', convert=1, nms=False)

pog_manager = POGManager()
image_list = glob.glob('test_images/*.jpg')

for image_name in image_list:
    # image preprocessing - keras_retinanet
    image = read_image_bgr(image_name)
    detection_output_image = image.copy()
    cluster_output_image = image.copy()
    image = preprocess_image(image)
    image, scale = resize_image(image)
    boxes, scores, labels = predict(image, scale, image_name, model)
    group_boxes = util.find_groups(detection_output_image, boxes)

    img = cv2.cvtColor(cluster_output_image, cv2.COLOR_BGR2RGB)
    pil_img = Image.fromarray(img)
    for loc, groups in group_boxes.items():
        seg, shelf = loc
        product_list = pog_manager.get_product_list(seg, shelf)
        product_list = product_list[:len(groups)]
        for i in range(len(product_list)):
            product = product_list[i]
            box = groups[i]
            draw_bbox(pil_img, box)
            draw_name(pil_img, box, product.name)
    pil_img.save(os.path.join(cluster_output_dir, os.path.basename(image_name)), 'JPEG')
    draw_detections(detection_output_image, np.asarray(boxes), np.asarray(scores), np.asarray(labels), (255, 0, 0))
    cv2.imwrite(os.path.join(detection_output_dir, os.path.basename(image_name)), detection_output_image)
